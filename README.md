Spring Metrics
--------------
Spring Metrics is a simple demonstration project to explore various metric gathering 
tools that can be used to gain a better understanding of Spring Boot applications are 
doing. This project focuses on Spring Actuator, Micrometer Prometheus and Grafana.

### Requirements
The Spring Metrics application makes use of several external tools that must be installed
and configured before you can use it.

1. Micrometer Prometheus (https://prometheus.io/)
   - Prometheus can be installed to your local machine easily with Docker. First pull
     the latest image with:
     ```
     docker pull prom/prometheus
     ```  
   
   - Once the image has been pulled, open the `prometheus-config.yml` file located under 
     the `resources` directory and replace `<HOST>` in `targets: ['<HOST>:8080']` with
     your IP address.
     - Because Prometheus is running in Docker, you cannot use http://localhost as your
       `<HOST>`.
       
   - Finally Prometheus can be run with the following command where `<PATH_TO_CONFIG>`
     is and absolute path to the `prometheus-config.yml` file.
     ```
     docker run -d \
         --name=Prometheus \
         -p 9090:9090 \
         -v <PATH_TO_CONFIG>:/etc/prometheus/prometheus.yml prom/prometheus \
         --config.file=/etc/prometheus/prometheus.yml 
     ```

2. Grafana (https://grafana.com/)
   - Grafana can be installed to your local machine easily with Docker. First pull the 
     latest image with: 
     ```
     docker pull grafana/grafana
     ```
     
   - Once the image has been pulled, it can be run with the following:
     ```
     docker run -d \
         -p 9091:3000 \
         grafana/grafana
     ```
     
   - Once you have Grafana running you will need to setup a Prometheus data source. First
     go to http://localhost:9091 and sign in to the Grafana dashboard with the following
     credentials:
     ```
     username: admin
     password: admin
     ```
   
   - Next create the data source by hovering over the settings button and clicking on 
     `Data Sources`. Next click on the `Add Data Source` button and select the 
     `Prometheus` option. Enter the address Prometheus is running on 
     (http://<YOUR_IP>:9090) and click on `Save & Test`.
   
   - Finally create a new Grafana dashboard by hovering over the `+` button and clicking
     on the `Import` button. Enter `4701` in the Grafana.com Dashboard field to make use 
     of Micrometers pre-built JVM dashboard. Next select the data source you created
     earlier with the `Prometheus` option and click on `Import`.
     
### Build & Run
The application comes with a random traffic generator to simulate requests that can be
viewed on Grafana. To generate this random traffic, simply send a GET request to 
http://localhost:8080/api/spring-metrics/generate/500 which would generate 500 random
requests. 