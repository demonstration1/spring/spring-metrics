package me.liammcguire.demobox.spring.springmetrics

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange

@SpringBootApplication
@RestController
@RequestMapping("/api/spring-metrics")
class SpringMetricsApplication {

    @Bean
    fun restTemplate(): RestTemplate = RestTemplateBuilder().build()

    @GetMapping("/generate/{numberOfRequests}")
    fun generateTraffic(
        @PathVariable("numberOfRequests") numberOfRequests: Int
    ): ResponseEntity<Void> {
        val restTemplate = restTemplate()
        logger.info("Sending $numberOfRequests random requests to /api/spring-metrics")
        for (i in 0..numberOfRequests) {
            try {
                val request = randomRequest()
                restTemplate.exchange<Void>(request.first, request.second, null)
            } catch (e: Exception) {
                // We don't care about exceptions.
            }
        }
        logger.info("Finished sending $numberOfRequests random requests to /api/spring-metrics")
        return ResponseEntity.status(HttpStatus.OK).build()
    }

    @GetMapping("/list")
    fun list(): ResponseEntity<Void> = ResponseEntity
        .status(randomHttpStatus())
        .contentType(MediaType.APPLICATION_JSON)
        .build()

    @PostMapping("/create")
    fun post(): ResponseEntity<Void> = ResponseEntity
        .status(randomHttpStatus())
        .contentType(MediaType.APPLICATION_JSON)
        .build()

    @PutMapping("/update")
    fun put(): ResponseEntity<Void> = ResponseEntity
        .status(randomHttpStatus())
        .contentType(MediaType.APPLICATION_JSON)
        .build()

    @DeleteMapping("/delete")
    fun delete(): ResponseEntity<Void> = ResponseEntity
        .status(randomHttpStatus())
        .contentType(MediaType.APPLICATION_JSON)
        .build()

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(SpringMetricsApplication::class.java)

        private fun randomHttpStatus(): HttpStatus = listOf(
            HttpStatus.OK,
            HttpStatus.BAD_REQUEST,
            HttpStatus.UNAUTHORIZED,
            HttpStatus.INTERNAL_SERVER_ERROR
        ).random()

        private fun randomRequest(): Pair<String, HttpMethod> = listOf(
            Pair("http://localhost:8080/api/foo/list", HttpMethod.GET),
            Pair("http://localhost:8080/api/foo/create", HttpMethod.POST),
            Pair("http://localhost:8080/api/foo/update", HttpMethod.PUT),
            Pair("http://localhost:8080/api/foo/delete", HttpMethod.DELETE)
        ).random()
    }
}

fun main(args: Array<String>) {
    runApplication<SpringMetricsApplication>(*args)
}
